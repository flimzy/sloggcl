package sloggcl

import (
	"log/slog"
	"testing"

	"cloud.google.com/go/logging"
	"github.com/google/go-cmp/cmp"
)

func TestGCLHandler_createEntry(t *testing.T) {
	tests := []struct {
		name   string
		record slog.Record
		want   *logging.Entry
	}{
		{
			name: "message only",
			record: slog.Record{
				Message: "message",
			},
			want: &logging.Entry{
				Severity: logging.Info,
				Payload:  "{\"msg\":\"message\"}\n",
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &GCLHandler{
				opts: &HandlerOptions{},
			}
			got := h.createEntry(tt.record)
			if d := cmp.Diff(tt.want, got); d != "" {
				t.Error(d)
			}
		})
	}
}
