// Package sloggcl provides an [log/slog.Handler] implementation that sends logs
// to the [Google Cloud Logging] service, via the [cloud.google.com/go/logging]
// package.
//
// [Google Cloud Logging]: https://cloud.google.com/logging/
// [cloud.google.com/go/logging]: https://pkg.go.dev/cloud.google.com/go/logging
package sloggcl
