package sloggcl

import (
	"context"
	"log/slog"
	"slices"

	"cloud.google.com/go/logging"
	"google.golang.org/api/option"
)

// HandlerOptions are options for a GCLHandler. A zero HandlerOptions consists
// entirely of default values.
type HandlerOptions = slog.HandlerOptions

// New connects to the Google Cloud Logging service, and returns a new handler.
// ctx, parent, and clientOpts are passed directly to
// [cloud.google.com/go/logging.NewClient].
func New(ctx context.Context, opts *HandlerOptions, parent string, clientOpts ...option.ClientOption) (*GCLHandler, error) {
	client, err := logging.NewClient(ctx, parent, clientOpts...)
	if err != nil {
		return nil, err
	}
	return NewFromClient(client, opts), nil
}

// NewFromClient creates a new handler from an existing connection to the
// Google Cloud Logging service.
func NewFromClient(client *logging.Client, opts *HandlerOptions) *GCLHandler {
	return &GCLHandler{
		opts:   opts,
		client: client,
	}
}

// GCLHandler is a [log/slog.Handler] that sends logs to [Google Cloud Logging].
//
// [Google Cloud Logging]: https://cloud.google.com/logging/
type GCLHandler struct {
	opts              *HandlerOptions
	client            *logging.Client
	preformattedAttrs []byte
	groups            []string
	nOpenGroups       int // the number of groups opened in preformattedAttrs
}

var _ slog.Handler = (*GCLHandler)(nil)

func (h *GCLHandler) clone() *GCLHandler {
	return &GCLHandler{
		opts:              h.opts,
		preformattedAttrs: slices.Clip(h.preformattedAttrs),
		groups:            h.groups,
		client:            h.client,
	}
}

// Enabled reports whether level is greater than or equal to the minimum level.
func (h *GCLHandler) Enabled(_ context.Context, level slog.Level) bool {
	minLevel := slog.LevelInfo
	if h.opts.Level != nil {
		minLevel = h.opts.Level.Level()
	}
	return level >= minLevel
}

// Handle handles a logging event.
func (h *GCLHandler) Handle(_ context.Context, r slog.Record) error { //nolint:gocritic // heavy param required by interface
	_ = h.createEntry(r)
	return nil
}

var level2severity = map[slog.Level]logging.Severity{
	slog.LevelDebug: logging.Debug,
	slog.LevelInfo:  logging.Info,
	slog.LevelWarn:  logging.Warning,
	slog.LevelError: logging.Error,
}
