[run]
timeout = "900s"

[output]
format = "colored-line-number"

[linters]
disable-all = true
enable = [
  "unused",                    # Detects unused code
  "unparam",                   # Detect unused function parameters that can be removed
  "dupword",                   # checks for duplicate words in the source code
  "errname",                   # Ensure error constants and variables are properly named
  "gci",                       # Controls import order and makes it always deterministic
  "gofumpt",                   # gofumpt is a stricter version of gofmt
  "misspell",                  # Catch misspelled words
  "nestif",                    # Reports deeply-nested if statements
  "unconvert",                 # Unnecessary type conversions
  "gosimple",                  # Various code simplifications
  "errcheck",                  # Unchecked errors
  "errchkjson",                # Failure to check JSON-related errors
  "errorlint",                 # Detect error-wrapping related bugs
  "gocritic",                  # See [linters-settings.gocritic] below for details
  "gocheckcompilerdirectives", # Checks that go compiler directive comments (//go:) are valid # likely dupe of gofumpt
  "gosec",                     # A number of security-related checks
  "ineffassign",               # Variable assignments that are never used
  "makezero",                  # Detects improper creation of slices with non-zero length
  "nilerr",                    # Finds the code that returns nil even if it checks that the error is not nil
  "nilnil",                    # Checks that there is no simultaneous return of `nil` error and an invalid value
  "noctx",                     # Detects HTTP requests without context
  "reassign",                  # Checks that package variables are not reassigned
  "revive",                    # See [linters-settings.revive] below for details
  "staticcheck",               # Various bug and other style checks
  "tenv",                      # Catches use of os.Setenv in tests
  "testableexamples",          # checks if examples are testable
  "thelper",                   # Catches failure to call t.Helper() in testing helper funcs
  "tparallel",                 # Catches failure to call t.Parallel() in tests
  "prealloc",                  # Detects when slices can be pre-allocated to reduce memory pressure
  "govet",                     # The default `go vet` tool from the Go toolchain
  "usestdlibvars",             # Instructs you to use stdlib vars and constants where possible
  "dupl",                      # Detects duplicate code blocks
  "promlinter",                # Check Prometheus metrics naming
]

[issues]
exclude-use-default = false
new-from-rev = "7435e3a54a0e9cb8816eac427fe8ecad57767198"

# Allow common Mongo strings
[[issues.exclude-rules]]
text = "add-constant: string literal \"(\\$in|\\$lt|\\$elemMatch|\\$match|\\$eq|-_id)\""
linters = ["revive"]

# Allow repeating log messages
[[issues.exclude-rules]]
text = "add-constant: string literal"
source = "\\.(Info|Warn|Error|Debug)f?\\("
linters = ["revive"]

[[issues.exclude-rules]]
text = "declaration of \"err\" shadows"
linters = ["govet"]

[[issues.exclude-rules]]
path = ".*_test.go"
text = "fieldalignment:"

[[issues.exclude-rules]]
path = ".*_test.go"
source = "tests.Run\\(t, func\\(t \\*testing\\.T, "
linters = ["thelper"]

[[issues.exclude-rules]]
path = ".*_test.go"
source = "tests.Add\\("
linters = ["thelper"]

[[issues.exclude-rules]]
source = "struct{}"
text = "no nested structs are allowed"
linters = ["revive"]

[[issues.exclude-rules]]
text = " is not checked" # This check is duplicated by errcheck, and errcheck is smarter
linters = ["errchkjson"]

[[issues.exclude-rules]]
source = "defer .*\\.Close\\(\\)$"
linters = ["errcheck"]

[linters-settings.gosec]
excludes = [
  "G104", # Duplicate of errcheck
  "G307", # Duplicate of errcheck
]

[linters-settings.gocritic]
enabled-checks = [
  # To see all available checks, run `GL_DEBUG=gocritic golangci-lint run`

  ## Unused code
  "commentedOutCode",   # Detects commented-out code inside function bodies
  "commentedOutImport", # Detects commented-out imports
  "unlabelStmt",        # Detects redundant statement labels

  ## Code formatting & style
  "assignOp",              # Detects assignments that can be simplified by using assignment operators
  "captLocal",             # Detects capitalized names for local variables
  "defaultCaseOrder",      # Detects when default case in switch isn’t on 1st or last position
  "deprecatedComment",     # Detects malformed ‘deprecated’ doc-comments
  "docStub",               # Detects comments that silence go lint complaints about doc-comment
  "dupImport",             # Detects multiple imports of the same package under different aliases
  "elseif",                # Detects else with nested if statement that can be replaced with else-if
  "emptyDecl",             # Detects suspicious empty declarations blocks
  "emptyStringTest",       # Detects empty string checks that can be written more idiomatically
  "exposedSyncMutex",      # Detects exposed methods from sync.Mutex and sync.RWMutex
  "hexLiteral",            # Detects hex literals that have mixed case letter digits
  "ifElseChain",           # Detects repeated if-else statements and suggests to replace them with switch statement
  "preferFilepathJoin",    # Detects concatenation with os.PathSeparator which can be replaced with filepath.Join
  "ptrToRefParam",         # Detects input and output parameters that have a type of pointer to referential type
  "timeExprSimplify",      # Detects manual conversion to milli- or microseconds
  "typeAssertChain",       # Detects repeated type assertions and suggests to replace them with type switch statement.
  "whyNoLint",             # Ensures that //nolint comments include an explanation // replaces nolintlint
  "yodaStyleExpr",         # Detects Yoda style expressions and suggests to replace them
  "appendCombine",         # Detects append chains to the same slice that can be done in a single append call
  "boolExprSimplify",      # Detects bool expressions that can be simplified.
  "deferUnlambda",         # Detects deferred function literals that can be simplified
  "sloppyTypeAssert",      # Detects redundant type assertions
  "unnecessaryDefer",      # Detects redundantly deferred calls
  "emptyFallthrough",      # Detects fallthrough that can be avoided by using multi case values
  "methodExprCall",        # Detects method expression call that can be replaced with a method call
  "nestingReduce",         # Finds where nesting level could be reduced
  "newDeref",              # Detects immediate dereferencing of new expressions
  "paramTypeCombine",      # Detects if function parameters could be combined by type and suggest the way to do it
  "preferFprint",          # Detects fmt.Sprint(f/ln) calls which can be replaced with fmt.Fprint(f/ln)
  "redundantSprint",       # Detects redundant fmt.Sprint calls
  "regexpSimplify",        # Detects regexp patterns that can be simplified
  "singleCaseSwitch",      # Detects switch statements that could be better written as if statement
  "sloppyLen",             # Detects usage of len when result is obvious or doesn’t make sense
  "stringConcatSimplify",  # Detects string concat operations that can be simplified
  "stringsCompare",        # Detects strings.Compare usage
  "switchTrue",            # Detects switch-over-bool statements that use explicit true tag value
  "timeCmpSimplify",       # Detects Before/After call of time.Time that can be simplified
  "typeSwitchVar",         # Detects type switches that can benefit from type guard clause with variable
  "typeUnparen",           # Detects unneded parenthesis inside type expressions and suggests to remove them
  "underef",               # Detects dereference expressions that can be omitted
  "unlambda",              # Detects function literals that can be simplified
  "unnecessaryBlock",      # Detects unnecessary braced statement blocks
  "unslice",               # Detects slice expressions that can be simplified to sliced expression itself
  "valSwap",               # Detects value swapping code that are not using parallel assignment
  "wrapperFunc",           # Detects function calls that can be replaced with convenience wrappers
  "argOrder",              # Detects suspicious arguments order
  "badCall",               # Detects suspicious function calls
  "badCond",               # Detects suspicious condition expressions
  "badLock",               # Detects suspicious mutex lock/unlock operations
  "badRegexp",             # Detects suspicious regexp patterns
  "badSorting",            # Detects bad usage of sort package
  "caseOrder",             # Detects erroneous case order inside switch statements
  "deferInLoop",           # Detects loops inside functions that use defer
  "dupArg",                # Detects suspicious duplicated arguments
  "dupBranchBody",         # Detects duplicated branch bodies inside conditional statements
  "dupCase",               # Detects duplicated case clauses inside switch or select statements
  "dupSubExpr",            # Detects suspicious duplicated sub-expressions
  "dynamicFmtString",      # Detects suspicious formatting strings usage
  "evalOrder",             # Detects unwanted dependencies on the evaluation order
  "exitAfterDefer",        # Detects calls to exit/fatal inside functions that use defer
  "externalErrorReassign", # Detects suspicious reassigment of error from another package
  "flagDeref",             # Detects immediate dereferencing of flag package pointers
  "flagName",              # Detects suspicious flag names
  "initClause",            # Detects non-assignment statements inside if/switch init clause
  "mapKey",                # Detects suspicious map literal keys
  "nilValReturn",          # Detects return statements those results evaluate to nil
  "offBy1",                # Detects various off-by-one kind of errors
  "regexpMust",            # Detects regexp.Compile* that can be replaced with regexp.MustCompile*
  "regexpPattern",         # Detects suspicious regexp patterns
  "returnAfterHttpError",  # Detects suspicious http.Error call without following return
  "sortSlice",             # Detects suspicious sort.Slice calls
  "truncateCmp",           # Detects potential truncation issues when comparing ints of different sizes
  "uncheckedInlineErr",    # Detects unchecked errors in if statements
  "weakCond",              # Detects conditions that are unsafe due to not being exhaustive.
  "equalFold",             # Detects unoptimal strings/bytes case-insensitive comparison
  "hugeParam",             # Detects params that incur excessive amount of copying
  "indexAlloc",            # Detects strings.Index calls that may cause unwanted allocs
  "preferDecodeRune",      # Detects expressions like []rune(s)[0] that may cause unwanted rune slice allocation
  "preferStringWriter",    # Detects w.Write or io.WriteString calls which can be replaced with w.WriteString
  "preferWriteByte",       # Detects WriteRune calls with rune literal argument that is single byte and reports to use WriteByte instead
  "rangeExprCopy",         # Detects expensive copies of for loop range expressions
  "sliceClear",            # Detects slice clear loops, suggests an idiom that is recognized by the Go compiler
  "stringXbytes",          # Detects redundant conversions between string and []byte
]

[linters-settings.prealloc]
for-loops = true

[linters-settings.nolintlint]
allow-unused = false
allow-leading-space = false
require-explanation = true
require-specific = true

[linters-settings.errcheck]
check-type-assertions = true

[linters-settings.govet]
check-shadowing = true
enable-all = true
disable = ["fieldalignment"]

[linters-settings.revive]
max-open-files = 2_048
ignore-generated-header = true
severity = "error"
# Enable all revive rules, except those we disable explicitly below.
# The full list of available rules can be found at
# https://github.com/mgechev/revive/blob/master/RULES_DESCRIPTIONS.md
enable-all-rules = true
confidence = 0.1

# Suggests using constant for magic numbers and string literals.
[[linters-settings.revive.rules]]
name = "add-constant"
disabled = false

# Warns when a function receives more parameters than the maximum set by the rule's configuration. Enforcing a maximum number of parameters helps to keep the code readable and maintainable.
[[linters-settings.revive.rules]]
name = "argument-limit"
disabled = false
arguments = [10]

# Check for commonly mistaken usages of the sync/atomic package.
[[linters-settings.revive.rules]]
name = "atomic"
disabled = false

# Checks given banned characters in identifiers(func, var, const). Comments are not checked.
[[linters-settings.revive.rules]]
name = "banned-characters"
disabled = true

# Warns on bare (a.k.a. naked) returns.
[[linters-settings.revive.rules]]
name = "bare-return"
disabled = false

# Blank import should be only in a main or test package, or have a comment justifying it.
[[linters-settings.revive.rules]]
name = "blank-imports"
disabled = false

# Using Boolean literals (true, false) in logic expressions may make the code less readable. This rule suggests removing Boolean literals from logic expressions.
[[linters-settings.revive.rules]]
name = "bool-literal-in-expr"
disabled = false

# Explicitly invoking the garbage collector is, except for specific uses in benchmarking, very dubious.
[[linters-settings.revive.rules]]
name = "call-to-gc"
disabled = false

# Cognitive complexity is a measure of how hard code is to understand. While cyclomatic complexity is good to measure "testability" of the code, cognitive complexity aims to provide a more precise measure of the difficulty of understanding the code. Enforcing a maximum complexity per function helps to keep code readable and maintainable.
[[linters-settings.revive.rules]]
name = "cognitive-complexity"
disabled = true

# Spots comments with improper spacing.
[[linters-settings.revive.rules]]
name = "comment-spacings"
disabled = true                         # gofumpt should already handle this for us
arguments = ["mypragma", "otherpragma"]

# Methods or fields of struct that have names different only by capitalization could be confusing.
[[linters-settings.revive.rules]]
name = "confusing-naming"
disabled = true

# Function or methods that return multiple, no named, values of the same type could induce error.
[[linters-settings.revive.rules]]
name = "confusing-results"
disabled = false

# The rule spots logical expressions that evaluate always to the same value.
[[linters-settings.revive.rules]]
name = "constant-logical-expr"
disabled = false

# By convention, context.Context should be the first parameter of a function. This rule spots function declarations that do not follow the convention.
[[linters-settings.revive.rules]]
name = "context-as-argument"
disabled = false

# Basic types should not be used as a key in context.WithValue.
[[linters-settings.revive.rules]]
name = "context-keys-type"
disabled = false

# Cyclomatic complexity is a measure of code complexity. Enforcing a maximum complexity per function helps to keep code readable and maintainable.
[[linters-settings.revive.rules]]
name = "cyclomatic"
disabled = true
arguments = [10]

# This rule spots potential dataraces caused by go-routines capturing (by-reference) particular identifiers of the function from which go-routines are created. The rule is able to spot two of such cases: go-routines capturing named return values, and capturing for-range values.
[[linters-settings.revive.rules]]
name = "datarace"
disabled = false

# Packages exposing functions that can stop program execution by exiting are hard to reuse. This rule looks for program exits in functions other than main() or init().
[[linters-settings.revive.rules]]
name = "deep-exit"
disabled = false

# This rule warns on some common mistakes when using defer statement.
[[linters-settings.revive.rules]]
name = "defer"
disabled = false

# Importing with . makes the programs much harder to understand because it is unclear whether names belong to the current package or to an imported package.
[[linters-settings.revive.rules]]
name = "dot-imports"
disabled = false

# It is possible to unintentionally import the same package twice. This rule looks for packages that are imported two or more times.
[[linters-settings.revive.rules]]
name = "duplicated-imports"
disabled = true             # Duplicate of gocritic's dupImport

# In Go it is idiomatic to minimize nesting statements, a typical example is to avoid if-then-else constructions.
[[linters-settings.revive.rules]]
name = "early-return"
disabled = false

# Empty blocks make code less readable and could be a symptom of a bug or unfinished refactoring.
[[linters-settings.revive.rules]]
name = "empty-block"
disabled = false

# Sometimes gofmt is not enough to enforce a common formatting of a code-base; this rule warns when there are heading or trailing newlines in code blocks.
[[linters-settings.revive.rules]]
name = "empty-lines"
disabled = false

# By convention, for the sake of readability, variables of type error must be named with the prefix err.
[[linters-settings.revive.rules]]
name = "error-naming"
disabled = false

# By convention, for the sake of readability, the errors should be last in the list of returned values by a function.
[[linters-settings.revive.rules]]
name = "error-return"
disabled = false

# By convention, for better readability, error messages should not be capitalized or end with punctuation or a newline.
[[linters-settings.revive.rules]]
name = "error-strings"
disabled = false

# It is possible to get a simpler program by replacing errors.New(fmt.Sprintf()) with fmt.Errorf(). This rule spots that kind of simplification opportunities.
[[linters-settings.revive.rules]]
name = "errorf"
disabled = false

# Exported function and methods should have comments. This warns on undocumented exported functions and methods.
[[linters-settings.revive.rules]]
name = "exported"
disabled = false
arguments = ["sayRepetitiveInsteadOfStutters"]

#  This rule helps to enforce a common header for all source files in a project by spotting those files that do not have the specified header.
[[linters-settings.revive.rules]]
name = "file-header"
disabled = true      # No standard file header for this project

# If a function controls the flow of another by passing it information on what to do, both functions are said to be control-coupled. Coupling among functions must be minimized for better maintainability of the code. This rule warns on boolean parameters that create a control coupling.
[[linters-settings.revive.rules]]
name = "flag-parameter"
disabled = false

# Functions returning too many results can be hard to understand/use.
[[linters-settings.revive.rules]]
name = "function-result-limit"
disabled = false
arguments = [3]

# Functions too long (with many statements and/or lines) can be hard to understand.
[[linters-settings.revive.rules]]
name = "function-length"
disabled = true
arguments = [25, 0]

# Typically, functions with names prefixed with Get are supposed to return a value.
[[linters-settings.revive.rules]]
name = "get-return"
disabled = false

# an if-then-else conditional with identical implementations in both branches is an error.
[[linters-settings.revive.rules]]
name = "identical-branches"
disabled = false

# Checking if an error is nil to just after return the error or nil is redundant.
[[linters-settings.revive.rules]]
name = "if-return"
disabled = false

# By convention, for better readability, incrementing an integer variable by 1 is recommended to be done using the ++ operator. This rule spots expressions like i += 1 and i -= 1 and proposes to change them into i++ and i--.
[[linters-settings.revive.rules]]
name = "increment-decrement"
disabled = false

# To improve the readability of code, it is recommended to reduce the indentation as much as possible. This rule highlights redundant else-blocks that can be eliminated from the code.
[[linters-settings.revive.rules]]
name = "indent-error-flow"
disabled = false

# Warns when importing black-listed packages.
[[linters-settings.revive.rules]]
name = "imports-blacklist"
disabled = true

# In Go it is possible to declare identifiers (packages, structs, interfaces, parameters, receivers, variables, constants...) that conflict with the name of an imported package. This rule spots identifiers that shadow an import.
[[linters-settings.revive.rules]]
name = "import-shadowing"
disabled = false

# Warns in the presence of code lines longer than a configured maximum.
[[linters-settings.revive.rules]]
name = "line-length-limit"
disabled = false
arguments = [150]

# Packages declaring too many public structs can be hard to understand/use, and could be a symptom of bad design.
[[linters-settings.revive.rules]]
name = "max-public-structs"
disabled = true             # Silly rule, IMO

# A function that modifies its parameters can be hard to understand. It can also be misleading if the arguments are passed by value by the caller. This rule warns when a function modifies one or more of its parameters.
[[linters-settings.revive.rules]]
name = "modifies-parameter"
disabled = true             # Too many false positives

# A method that modifies its receiver value can have undesired behavior. The modification can be also the root of a bug because the actual value receiver could be a copy of that used at the calling site. This rule warns when a method modifies its receiver.
[[linters-settings.revive.rules]]
name = "modifies-value-receiver"
disabled = false

# Packages declaring structs that contain other inline struct definitions can be hard to understand/read for other developers.
[[linters-settings.revive.rules]]
name = "nested-structs"
disabled = false

# Conditional expressions can be written to take advantage of short circuit evaluation and speed up its average evaluation time by forcing the evaluation of less time-consuming terms before more costly ones. This rule spots logical expressions where the order of evaluation of terms seems non optimal. Please notice that confidence of this rule is low and is up to the user to decide if the suggested rewrite of the expression keeps the semantics of the original one.
[[linters-settings.revive.rules]]
name = "optimize-operands-order"
disabled = true                  # too many wrong suggestions

# Packages should have comments. This rule warns on undocumented packages and when packages comments are detached to the package keyword.
[[linters-settings.revive.rules]]
name = "package-comments"
disabled = true           # Buggy, complains about every file

# This rule suggests a shorter way of writing ranges that do not use the second value.
[[linters-settings.revive.rules]]
name = "range"
disabled = false

# Range variables in a loop are reused at each iteration; therefore a goroutine created in a loop will point to the range variable with from the upper scope. This way, the goroutine could use the variable with an undesired value. This rule warns when a range value (or index) is used inside a closure.
[[linters-settings.revive.rules]]
name = "range-val-in-closure"
disabled = true               # Obsoleted by GOEXPERIMENT=loopvar

# Range variables in a loop are reused at each iteration. This rule warns when assigning the address of the variable, passing the address to append() or using it in a map.
[[linters-settings.revive.rules]]
name = "range-val-address"
disabled = true            # Obsoleted by GOEXPERIMENT=loopvar

# By convention, receiver names in a method should reflect their identity. For example, if the receiver is of type Parts, p is an adequate name for it. Contrary to other languages, it is not idiomatic to name receivers as this or self.
[[linters-settings.revive.rules]]
name = "receiver-naming"
disabled = false

# Constant names like false, true, nil, function names like append, make, and basic type names like bool, and byte are not reserved words of the language; therefore the can be redefined. Even if possible, redefining these built in names can lead to bugs very difficult to detect.
[[linters-settings.revive.rules]]
name = "redefines-builtin-id"
disabled = false

# explicit type conversion string(i) where i has an integer type other than rune might behave not as expected by the developer (e.g. string(42) is not "42"). This rule spot that kind of suspicious conversions.
[[linters-settings.revive.rules]]
name = "string-of-int"
disabled = false

# This rule allows you to configure a list of regular expressions that string literals in certain function calls are checked against. This is geared towards user facing applications where string literals are often used for messages that will be presented to users, so it may be desirable to enforce consistent formatting.
[[linters-settings.revive.rules]]
name = "string-format"
disabled = true        # Probably worth configuring this later
# arguments = [ ... ]

# Struct tags are not checked at compile time. This rule, checks and warns if it finds errors in common struct tags types like: asn1, default, json, protobuf, xml, yaml.
[[linters-settings.revive.rules]]
name = "struct-tag"
disabled = false

# To improve the readability of code, it is recommended to reduce the indentation as much as possible. This rule highlights redundant else-blocks that can be eliminated from the code.
[[linters-settings.revive.rules]]
name = "superfluous-else"
disabled = false

# his rule warns when using == and != for equality check time.Time and suggest to time.time.Equal method.
[[linters-settings.revive.rules]]
name = "time-equal"
disabled = false

# Using unit-specific suffix like "Secs", "Mins", ... when naming variables of type time.Duration can be misleading, this rule highlights those cases.
[[linters-settings.revive.rules]]
name = "time-naming"
disabled = false

# This rule warns when initialism, variable or package naming conventions are not followed.
[[linters-settings.revive.rules]]
name = "var-naming"
disabled = false

# This rule proposes simplifications of variable declarations.
[[linters-settings.revive.rules]]
name = "var-declaration"
disabled = false

# Unconditional recursive calls will produce infinite recursion, thus program stack overflow. This rule detects and warns about unconditional (direct) recursive calls.
[[linters-settings.revive.rules]]
name = "unconditional-recursion"
disabled = false

# This rule warns on wrongly named un-exported symbols, i.e. un-exported symbols whose name start with a capital letter.
[[linters-settings.revive.rules]]
name = "unexported-naming"
disabled = false

# This rule warns when an exported function or method returns a value of an un-exported type.
[[linters-settings.revive.rules]]
name = "unexported-return"
disabled = false

# This rule warns when errors returned by a function are not explicitly handled on the caller side.
[[linters-settings.revive.rules]]
name = "unhandled-error"
disabled = true          # duplicate of errcheck

# This rule suggests to remove redundant statements like a break at the end of a case block, for improving the code's readability.
[[linters-settings.revive.rules]]
name = "unnecessary-stmt"
disabled = false

# This rule spots and proposes to remove unreachable code.
[[linters-settings.revive.rules]]
name = "unreachable-code"
disabled = false

# This rule warns on unused parameters. Functions or methods with unused parameters can be a symptom of an unfinished refactoring or a bug.
[[linters-settings.revive.rules]]
name = "unused-parameter"
disabled = false

# This rule warns on unused method receivers. Methods with unused receivers can be a symptom of an unfinished refactoring or a bug.
[[linters-settings.revive.rules]]
name = "unused-receiver"
disabled = false

#  Since GO 1.18, interface{} has an alias: any. This rule proposes to replace instances of interface{} with any.
[[linters-settings.revive.rules]]
name = "use-any"
disabled = false

# This rule warns on useless break statements in case clauses of switch and select statements.
[[linters-settings.revive.rules]]
name = "useless-break"
disabled = false

# Function parameters that are passed by value, are in fact a copy of the original argument. Passing a copy of a sync.WaitGroup is usually not what the developer wants to do. This rule warns when a sync.WaitGroup expected as a by-value parameter in a function or method.
[[linters-settings.revive.rules]]
name = "waitgroup-by-value"
disabled = false

[linters-settings.usestdlibvars]
http-method = true
http-status-code = true
time-weekday = true
time-month = true
time-layout = true
crypto-hash = true
default-rpc-path = true

[linters-settings.gci]
sections = ["standard", "default", "prefix(gitlab.com/flimzy/sloggcl)"]
skip-generated = false
custom-order = true
